## network-data-generator.py

network-data-generator.py is a script that can be used for generating dummy network data.

The script uses Python3.4 and the excellent python package [Faker](https://pypi.org/project/Faker/).

It takes only one optional argument, the number of rows to generate (50 by default).

Usage:

    python3 network-data-generator.py 50

It also comes as a Docker container that can be used as follow:

    docker build -t network-data-generator .
    docker run --rm -it -v /host/path/:/script network-data-generator 50

By default, the script generates a CSV file "network-data.csv" with the following columns:

 - uuid
 - event_time
 - device_mac
 - device_ip
 - subnet
 - active
 - owner_id
 - owner_name
 - country 
 - address
 - building_code
