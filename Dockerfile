FROM python:3.4
LABEL maintainer="nathan.bigaignon@dmwgroup.co.uk"

RUN pip install faker
RUN mkdir /script
WORKDIR /script
COPY network-data-generator.py /script/
ENTRYPOINT ["python3", "/script/network-data-generator.py"]
