import csv
import sys
from faker import Factory
from faker.providers import internet


def main(rows=50):
    with open('network-data.csv', 'w') as csvfile:
        fieldnames = ['uuid', 'event_time', 'device_mac', 'device_ip', 'subnet',
                      'active', 'owner_id', 'owner_name', 'country', 'address', 'building_code', 'compliant']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        fake = Factory.create()
        fake.add_provider(internet)

        counter = 0
        while counter < rows:
            writer.writerow({'uuid': fake.uuid4(), 'event_time': fake.date_time(),
                             'device_mac': fake.mac_address(), 'device_ip': fake.ipv4(),
                             'subnet': fake.ipv4(network=True),
                             'active': fake.boolean(chance_of_getting_true=80),
                             'owner_id': fake.random_number(digits=6), 'owner_name': fake.name(),
                             'country': fake.country(), 'address': fake.address(),
                             'building_code': fake.building_number(),
                             'compliant': fake.boolean(chance_of_getting_true=70), })
            counter = counter + 1
    return 0


if __name__ == '__main__':
    if len(sys.argv) > 1:
        main(int(sys.argv[1]))
    else:
        main()
